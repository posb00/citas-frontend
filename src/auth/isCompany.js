import store from "../store";

export default (to, from, next) => {
  if (
    store.state.authData.isCompany &&
    to.path !== "/app/company/configuration"
  ) {
    next("/app/company/configuration");
  } else {
    next();
  }
};
