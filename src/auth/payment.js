import store from "../store";
import NProgress from "nprogress";

export default (to, from, next) => {
  store
    .dispatch("lastPayment")
    .then(response => {
      if (response.status === "Inactivo") {
        NProgress.done();
        next("/app/profile/subscriptions/subscription/1");
      } else {
        next();
      }
    })
    .catch(() => {
      NProgress.done();
    });
};
