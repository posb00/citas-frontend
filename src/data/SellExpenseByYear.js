import { Axios } from "@/data/config";

var dark_heading = "#c2c6dc";

async function loadIncomes() {
  return await Axios.get("api/monthlyIncomesByYear");
}

async function loadExpenses() {
  return await Axios.get("api/monthlyExpensesByYear").then(response => {
    return response.data;
  });
}

const incomes = loadIncomes();
const expenses = loadExpenses();

// start::echartBar
export const echartBar = {
  legend: {
    borderRadius: 0,
    orient: "horizontal",
    x: "right",
    data: ["Online", "Offline"]
  },
  grid: {
    left: "8px",
    right: "8px",
    bottom: "0",
    containLabel: true
  },
  tooltip: {
    show: true,
    backgroundColor: "rgba(0, 0, 0, .8)"
  },

  xAxis: [
    {
      type: "category",

      data: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec"
      ],
      axisTick: {
        alignWithLabel: true
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        color: dark_heading
      },
      axisLine: {
        show: true,
        color: dark_heading,

        lineStyle: {
          color: dark_heading
        }
      }
    }
  ],
  yAxis: [
    {
      type: "value",

      axisLabel: {
        color: dark_heading,
        formatter: "${value}"
      },
      axisLine: {
        show: false,
        color: dark_heading,

        lineStyle: {
          color: dark_heading
        }
      },
      min: 0,
      max: 5000,
      interval: 500,

      splitLine: {
        show: true,
        interval: "auto"
      }
    }
  ],

  series: [
    {
      name: "Ingresos",
      data: incomes,
      label: { show: false, color: "#0168c1" },
      type: "bar",
      barGap: 0,
      color: "#bcbbdd",
      smooth: true,
      itemStyle: {
        emphasis: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowOffsetY: -2,
          shadowColor: "rgba(0, 0, 0, 0.3)"
        }
      }
    },
    {
      name: "Gastos",
      data: expenses,
      label: { show: false, color: "#639" },
      type: "bar",
      color: "#7569b3",
      smooth: true,
      itemStyle: {
        emphasis: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowOffsetY: -2,
          shadowColor: "rgba(0, 0, 0, 0.3)"
        }
      }
    }
  ]
};
