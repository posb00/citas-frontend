import axios from "axios";

// export const Server = "http://127.0.0.1:8000/";
export const Server = "http://3.86.22.154/";

export let Axios = axios.create({
  baseURL: Server,
  headers: {
    Authorization: JSON.parse(localStorage.getItem("token"))
  }
});

Axios.interceptors.request.use(
  config => {
    let token = JSON.parse(localStorage.getItem("token"));

    if (token) {
      config.headers["Authorization"] = token;
    }

    return config;
  },

  error => {
    return Promise.reject(error);
  }
);
