import Vue from "vue";
import store from "./store";
// import { isMobile } from "mobile-device-detect";
import Router from "vue-router";
import NProgress from "nprogress";
import authenticate from "./auth/authenticate";
import payment from "./auth/payment";

Vue.use(Router);

// create new router

const routes = [
  {
    path: "/",
    component: () => import("./views/app"), //webpackChunkName app
    beforeEnter: authenticate,
    redirect: "/app/dashboards/dashboard.v1",

    children: [
      {
        path: "/app/dashboards",
        component: () => import("./views/app/dashboards"), //dashboard
        children: [
          {
            path: "dashboard.v1",
            component: () => import("./views/app/dashboards/dashboard.v1")
          },
          {
            path: "dashboard.v2",
            component: () => import("./views/app/dashboards/dashboard.v2")
          },
          {
            path: "dashboard.v3",
            component: () => import("./views/app/dashboards/dashboard.v3")
          },
          {
            path: "dashboard.v4",
            component: () => import("./views/app/dashboards/dashboard.v4")
          }
        ]
      },
      //Company
      {
        path: "/app/company",
        component: () => import("./views/app/company"),
        beforeEnter: authenticate,
        redirect: "/app/company/configuration",
        children: [
          {
            path: "configuration",
            component: () => import("./views/app/company/configuration"),
            beforeEnter: payment
          },
          {
            path: "counters",
            component: () => import("./views/app/company/counters/list")
          }
        ]
      },
      //Profile
      {
        path: "app/profile",
        component: () => import("./views/app/profile"),
        beforeEnter: authenticate,
        children: [
          {
            path: "profile",
            component: () => import("./views/app/profile/profile/"),
            children: [
              {
                path: "edit",
                component: () => import("./views/app/profile/profile/edit")
              }
            ]
          },
          {
            path: "subscriptions",
            component: () => import("./views/app/profile/subscriptions"),
            children: [
              {
                path: "subscription/:msg?",
                component: () =>
                  import("./views/app/profile/subscriptions/subscription")
              }
            ]
          }
        ]
      },
      //Reports
      {
        path: "app/reports",
        component: () => import("./views/app/reports"),
        beforeEnter: authenticate,
        children: [
          {
            path: "sells",
            component: () => import("./views/app/reports/sells")
          },
          {
            path: "incomes&expenses",
            component: () => import("./views/app/reports/incomes&expenses")
          },
          {
            path: "expenses",
            component: () => import("./views/app/reports/expenses")
          }
        ]
      },
      {
        path: "/app/payroll",
        component: () => import("./views/app/payroll"),
        beforeEnter: authenticate,
        redirect: "/app/payroll/employees",
        children: [
          {
            path: "employees",
            component: () => import("./views/app/payroll/employees"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/payroll/employees/list")
              }
            ]
          },
          {
            path: "positions",
            component: () => import("./views/app/payroll/positions"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/payroll/positions/list")
              }
            ]
          },
          {
            path: "departments",
            component: () => import("./views/app/payroll/departments"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/payroll/departments/list")
              }
            ]
          },
          {
            path: "schedules",
            component: () => import("./views/app/payroll/schedules"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/payroll/schedules/list")
              }
            ]
          }
        ]
      },
      //Inventory
      {
        path: "app/inventory",
        component: () => import("./views/app/inventory"),
        beforeEnter: authenticate,
        redirect: "app/inventory/categories",
        children: [
          {
            path: "products",
            component: () => import("./views/app/inventory/product/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/product/list")
              }
            ]
          },
          {
            path: "attributes",
            component: () => import("./views/app/inventory/attributes/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/attributes/list")
              }
            ]
          },
          {
            path: "attributevalues",
            component: () =>
              import("./views/app/inventory/attributevalues/list"),
            children: [
              {
                path: "list",
                component: () =>
                  import("./views/app/inventory/attributevalues/list")
              }
            ]
          },
          {
            path: "categories",
            component: () => import("./views/app/inventory/category/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/category/list")
              }
            ]
          },
          {
            path: "suppliers",
            component: () => import("./views/app/inventory/suppliers/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/suppliers/list")
              }
            ]
          },
          {
            path: "wharehouses",
            component: () => import("./views/app/inventory/wharehouses/list"),
            children: [
              {
                path: "list",
                component: () =>
                  import("./views/app/inventory/wharehouses/list")
              }
            ]
          },
          {
            path: "entry",
            component: () => import("./views/app/inventory/entry/"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/entry/list")
              },
              {
                path: "add",
                component: () => import("./views/app/inventory/entry/add"),
                beforeEnter: payment
              }
            ]
          },
          {
            path: "ajust",
            component: () => import("./views/app/inventory/ajust/modalForm")
          },
          {
            path: "units",
            component: () => import("./views/app/inventory/unit/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/inventory/unit/list")
              }
            ]
          }
        ]
      },
      //Expenses
      {
        path: "app/expenses",
        component: () => import("./views/app/expenses"),
        beforeEnter: authenticate,
        redirect: "app/expenses/expense",
        children: [
          {
            path: "expense",
            component: () => import("./views/app/expenses/expense/list")
          },
          {
            path: "expenseTypes",
            component: () => import("./views/app/expenses/expenseTypes/list")
          }
        ]
      },
      //Accounting
      {
        path: "app/accounting",
        component: () => import("./views/app/accounting"),
        beforeEnter: authenticate,
        redirect: "app/accounting/accounts",
        children: [
          {
            path: "accounts",
            component: () => import("./views/app/accounting/accounts/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/accounting/accounts/list")
              }
            ]
          },
          {
            path: "rates",
            component: () => import("./views/app/accounting/rates/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/accounting/rates/list")
              }
            ]
          },
          {
            path: "paymenttypes",
            component: () => import("./views/app/accounting/paymenttypes/list")
          },
          {
            path: "paymentmethods",
            component: () =>
              import("./views/app/accounting/paymentmethods/list")
          },
          {
            path: "paymentconditions",
            component: () =>
              import("./views/app/accounting/paymentconditions/list")
          }
        ]
      },
      //Incomes
      {
        path: "app/income",
        component: () => import("./views/app/income"),
        beforeEnter: authenticate,
        children: [
          {
            path: "invoice",
            component: () => import("./views/app/income/invoice"),
            children: [
              {
                path: "list",
                name: "invoice/list",
                props: true,
                component: () => import("./views/app/income/invoice/list")
              },
              {
                path: "add/:OrderId?",
                component: () => import("./views/app/income/invoice/add"),
                beforeEnter: payment
              }
            ]
          },
          {
            path: "orders",
            component: () => import("./views/app/income/orders"),
            children: [
              {
                path: "list",
                name: "orders/list",
                props: true,
                component: () => import("./views/app/income/orders/list")
              },
              {
                path: "add",
                component: () => import("./views/app/income/orders/add"),
                beforeEnter: payment
              }
            ]
          },
          {
            path: "client",
            component: () => import("./views/app/income/client/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/income/client/list")
              }
            ]
          },
          {
            path: "payments",
            component: () => import("./views/app/income/payments/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/income/payments/list")
              }
            ]
          },
          {
            path: "delivery",
            component: () => import("./views/app/income/delivery/list"),
            children: [
              {
                path: "list",
                component: () => import("./views/app/income/delivery/list")
              }
            ]
          },
          {
            path: "accountreceivables",
            component: () =>
              import("./views/app/income/accountreceivables/list"),
            children: [
              {
                path: "list",
                component: () =>
                  import("./views/app/income/accountreceivables/list")
              }
            ]
          }
        ]
      }
    ]
  },
  // sessions
  {
    path: "/app/sessions",
    component: () => import("./views/app/sessions"),
    redirect: "/app/sessions/signIn",
    children: [
      {
        path: "signIn",
        component: () => import("./views/app/sessions/signIn")
      },
      {
        path: "signUp",
        component: () => import("./views/app/sessions/signUp")
      },
      {
        path: "forgot",
        component: () => import("./views/app/sessions/forgot")
      }
    ]
  },

  {
    path: "/switch-menu",
    component: () => import("./containers/layouts/switchSidebar")
  },

  {
    path: "*",
    component: () => import("./views/app/pages/notFound")
  }
];

const router = new Router({
  mode: "history",
  linkActiveClass: "open",
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  // If this isn't an initial page load.
  if (to.path) {
    // Start the route progress bar.
    NProgress.start();
    NProgress.set(0.1);
  }

  next();
});

router.afterEach(() => {
  // Remove initial loading
  const gullPreLoading = document.getElementById("loading_wrap");
  if (gullPreLoading) {
    gullPreLoading.style.display = "none";
  }
  // Complete the animation of the route progress bar.
  setTimeout(() => NProgress.done(), 500);
  // NProgress.done();
  // if (isMobile) {
  if (window.innerWidth <= 1200) {
    // console.log("mobile");
    store.dispatch("changeSidebarProperties");
    if (store.getters.getSideBarToggleProperties.isSecondarySideNavOpen) {
      store.dispatch("changeSecondarySidebarProperties");
    }

    if (store.getters.getCompactSideBarToggleProperties.isSideNavOpen) {
      store.dispatch("changeCompactSidebarProperties");
    }
  } else {
    if (store.getters.getSideBarToggleProperties.isSecondarySideNavOpen) {
      store.dispatch("changeSecondarySidebarProperties");
    }

    // store.state.sidebarToggleProperties.isSecondarySideNavOpen = false;
  }
});

export default router;
