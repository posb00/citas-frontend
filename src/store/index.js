import Vuex from "vuex";
import Vue from "vue";
import largeSidebar from "./modules/largeSidebar";
import compactSidebar from "./modules/compactSidebar";
import chat from "./modules/chat";
import config from "./modules/config";
import authData from "./modules/authData";
import invoice from "./modules/invoiceApp";
import cart from "./modules/cart";
import entry from "./modules/entry";
import payment from "./modules/payment";
import reports from "./modules/reports";
import orders from "./modules/orders";


// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
  modules: {
    largeSidebar,
    compactSidebar,
    chat,
    config,
    authData,
    invoice,
    cart,
    entry,
    payment,
    reports,
    orders
  }
});
