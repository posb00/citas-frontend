import { Axios } from "@/data/config";

export default {
  state: {
    isCompany: false,
    permissions:
      localStorage.getItem("permissions") != null
        ? JSON.parse(localStorage.getItem("permissions"))
        : [],
    user:
      localStorage.getItem("user") != null
        ? JSON.parse(localStorage.getItem("user"))
        : [],
    loggedInUser:
      localStorage.getItem("token") != null
        ? JSON.parse(localStorage.getItem("token"))
        : null,
    loading: false,
    error: null
  },
  getters: {
    permissions: state => state.permissions,
    user: state => state.user,
    loggedInUser: state => state.loggedInUser,
    loading: state => state.loading,
    error: state => state.error
  },
  mutations: {
    setUser(state, data) {
      state.loggedInUser = data.token;
      state.permissions = data.permissions;
      state.user = data.user;
      state.loading = false;
      state.error = null;
    },
    editUser(state, user) {
      localStorage.setItem("user", JSON.stringify(user));
      state.user = user;
    },
    setLogout(state) {
      state.loggedInUser = null;
      state.permissions = [];
      state.user = [];
      state.loading = false;
      state.error = null;
      // this.$router.go("/");
    },
    setLoading(state, data) {
      state.loading = data;
      state.error = null;
    },
    setError(state, data) {
      state.error = data;
      state.loggedInUser = null;
      state.loading = false;
    },
    clearError(state) {
      state.error = null;
    },
    setCompany(state, data) {
      state.isCompany = data;
    }
  },
  actions: {
    isCompany({ commit }, data) {
      Axios.get("/api/companyDefault", {
        headers: {
          Authorization: data.token
        }
      }).then(response => {
        if (response.status == 200) {
          commit("setCompany", response.data);
        }
      });
    },
    login({ commit, dispatch }, data) {
      commit("clearError");
      commit("setLoading", true);
      Axios.post("api/login", { email: data.email, password: data.password })
        .then(user => {
          const newUserToken = "Bearer " + user.data.token;
          const newUser = user.data.user;
          const newPermissions = user.data.permissions;
          localStorage.setItem("token", JSON.stringify(newUserToken));
          localStorage.setItem("user", JSON.stringify(newUser));
          localStorage.setItem("permissions", JSON.stringify(newPermissions));
          commit("setUser", {
            token: user.data.token,
            user: user.data.user,
            permissions: user.data.permissions
          });
          dispatch("isCompany", { token: newUserToken });
        })
        .catch(function(error) {
          var message = null;
          var code = null;
          // Handle Errors here.
          if (error.response.data.msg) {
            message = error.response.data.msg;
            code = error.response.data.code;
          } else {
            message = error.response.statusText;
            code = error.response.status;
          }

          localStorage.removeItem("user");
          localStorage.removeItem("token");
          localStorage.removeItem("permissions");
          commit("setError", {
            message: message,
            code: code
          });
        });
    },

    signUserUp({ commit, dispatch }, data) {
      commit("setLoading", true);
      commit("clearError");
      Axios.post("api/register", {
        email: data.email,
        password: data.password,
        name: data.name,
        image: data.image
      })
        .then(user => {
          const newUserToken = "Bearer " + user.data.token;
          const newUser = user.data.user;
          const newPermissions = user.data.permissions;
          localStorage.setItem("token", JSON.stringify(newUserToken));
          localStorage.setItem("user", JSON.stringify(newUser));
          localStorage.setItem("permissions", JSON.stringify(newPermissions));
          commit("setUser", {
            token: user.data.token,
            user: user.data.user,
            permissions: user.data.permissions
          });
          dispatch("isCompany");
        })
        .catch(function(error) {
          var message = null;
          var code = null;
          // Handle Errors here.
          if (error.response.data.msg) {
            message = error.response.data.msg;
            code = error.response.data.code;
          } else {
            message = error.response.statusText;
            code = error.response.status;
          }

          localStorage.removeItem("user");
          localStorage.removeItem("token");
          localStorage.removeItem("permissions");
          commit("setError", {
            message: message,
            code: code
          });
        });
    },
    googleLogin() {
      Axios.get("/api/auth/google").then(response => {
        console.log(response);
      });
    },
    signOut({ commit }) {
      Axios.post("api/logout").then(() => {
        localStorage.removeItem("user");
        localStorage.removeItem("token");
        localStorage.removeItem("permissions");
        commit("setLogout");
      });
    }
  }
};
