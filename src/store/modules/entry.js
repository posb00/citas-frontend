import { Axios } from "@/data/config";

const state = {
  productList: [],
  selectedProduct: [
    {
      avatar: "pera",
      name: "pedro"
    }
  ]
};

const getters = {
  getProductLists: state => state.productList,
  getSelectedProduct: state => state.selectedProduct
};

const actions = {
  changeSelectedUser({ commit }, id) {
    commit("updateSelectedProduct", id);
  },
  getProductsList({ commit }) {
    Axios.get("/api/aiproducts").then(response => {
      commit("fillProducts", response.data);
    });
  }
};

const mutations = {
  updateSelectedProduct: (state, id) => {
    const sProduct = state.productList.filter(user => user.id == id);
    state.selectedProduct = sProduct[0];
  },
  fillProducts: (state, data) => {
    state.productList = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
