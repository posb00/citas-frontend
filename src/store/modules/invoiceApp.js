import { Axios } from "@/data/config";
import axios from "axios";

const state = {
  products: [],
  invoice: [],
  order: [],
  delivery: [],
  status: [],
  errors: [],
  paymentMethods: [],
  paymenttypes: [],
  employees: [],
  clients: [],
  items: [],
  modalPrintInvoice: false,
  invoiceConfig: []
};

const getters = {
  ModalPrintInvoice: state => state.modalPrintInvoice,
  Invoice: state => state.invoice,
  Order: state => state.order,
  DeliveryList: state => state.delivery,
  ProductsList: state => state.products,
  Errors: state => state.errors,
  Status: state => state.status,
  PaymenMethodsList: state => state.paymentMethods,
  PaymentTypesList: state => state.paymenttypes,
  EmployeesList: state => state.employees,
  ClientsList: state => state.clients,
  InvoiceConfigList: state => state.invoiceConfig,
  cartProducts: state => state.items,
  cartSubTotal: state => {
    return state.items.reduce((total, product) => {
      return total + product.price * product.quantity;
    }, 0);
  },
  cartItbis: state => {
    return state.items.reduce((total, product) => {
      return total + product.itbis;
    }, 0);
  }
};

const actions = {
  //INITIAL LOAD ALL DATA
  async initialLoad({ commit }) {
    commit("resetData");
    await axios
      .all([
        Axios.get("/api/aproducts"),
        Axios.get("/api/apaymenttypes"),
        Axios.get("/api/apaymentmethods"),
        Axios.get("/api/asemployees"),
        Axios.get("/api/aclients"),
        Axios.get("/api/invoiceconfig"),
        Axios.get("/api/adeliveries")
      ])
      .then(
        axios.spread(
          (
            products,
            paymenttypes,
            paymentmethods,
            employees,
            clients,
            InvoiceConfig,
            delivery
          ) => {
            commit("loadProducts", products.data);
            commit("loadPaymentTypes", paymenttypes.data);
            commit("loadPaymentMethods", paymentmethods.data);
            commit("loadEmployees", employees.data);
            commit("loadClients", clients.data);
            commit("loadInvoiceConfig", InvoiceConfig.data);
            commit("loadDelivery", delivery.data);
          }
        )
      );
  },

  async LoadOrder({ commit }, { id }) {
    const order = await Axios.get("api/orders/" + id);
    commit("loadOrderData", { order: order.data });
    return order;
  },
  //ADD Product TO CARD
  async addProductToCart({ state, commit }, { product, quantity }) {
    //FIND ITEM
    const cartItem = state.items.find(item => item.id === product.id);
    //IF ITEMS DONT EXIST PUSH
    if (!cartItem) {
      await commit("pushProductToCart", {
        product: product,
        quantity: quantity
      });
    }
    //IF ITEM EXIST INCREMENT
    else {
      await commit("incrementItemQuantity", { cartItem, quantity });
    }
  },

  //SAVE THE INVOICE
  async makeInvoices({ state, getters }, data) {
    return await Axios.post("api/invoices", {
      form: data,
      product: state.items,
      subtotal: getters.cartSubTotal,
      itbis: getters.cartItbis
    })
      .then(response => {
        state.items = [];
        return response;
      })
      .catch(error => {
        if (error.response) {
          if (422 === error.response.status) {
            state.errors = error.response.data.error;
            state.status = 422;
          }
        }
        return error.response;
      });
  },

  async makeOrders({ state, getters }, data) {
    return await Axios.post("api/orders", {
      form: data,
      product: state.items,
      subtotal: getters.cartSubTotal,
      itbis: getters.cartItbis
    })
      .then(response => {
        state.items = [];
        return response;
      })
      .catch(error => {
        if (error.response) {
          if (422 === error.response.status) {
            state.errors = error.response.data.error;
            state.status = 422;
          }
        }
        return error.response;
      });
  },

  async prepareForPrint({ commit }, data) {
    const invoice = await Axios.get("api/getPdfInvoice/" + data);
    await commit("loadInvoice", invoice.data);
  },

  async prepareForPrintOrder({ commit }, data) {
    const order = await Axios.get("api/getPdfOrder/" + data);
    await commit("loadOrderPrint", order.data);
  }
};

const mutations = {
  CloseInvoicePrintModal(state) {
    state.modalPrintInvoice = false;
  },
  OpenInvoicePrintModal(state) {
    state.modalPrintInvoice = true;
  },
  resetData(state) {
    state.items = [];
  },
  loadOrderData(state, { order }) {
    order.order_details.forEach(product => {
      state.items.push({
        id: product.product_id,
        rate_id: product.rate_id,
        rate: product.product_rate_value,
        description: product.product_description,
        code: product.product_code,
        name: product.product_name,
        change_price: Number(product.change_price),
        sell_price: Number(product.sell_price),
        base_price: Number(product.base_price),
        price: Number(product.product_price),
        sub_total: product.product_sub_total,
        itbis: Number(product.product_itbis),
        total: Number(product.product_total),
        quantity: Number(product.product_quantity)
      });
    });
  },
  pushProductToCart(state, { product, quantity }) {
    state.items.push({
      id: product.id,
      rate_id: product.rate_id,
      rate: product.rate.rate,
      description: product.description,
      code: product.code,
      name: product.name,
      change_price: product.change_price,
      sell_price: product.sell_price,
      base_price: product.base_price,
      price: Number(product.base_price),
      sub_total: Number(product.base_price * quantity),
      itbis: Number(
        product.sell_price * quantity - product.base_price * quantity
      ),
      total: Number(product.sell_price * quantity),
      quantity
    });
  },
  reset(state) {
    state.errors = [];
  },
  incrementItemQuantity(state, { cartItem, quantity }) {
    let newQuantity = Number(cartItem.quantity) + Number(quantity);
    cartItem.sub_total = cartItem.base_price * newQuantity;
    cartItem.itbis =
      cartItem.sell_price * newQuantity - cartItem.base_price * newQuantity;
    cartItem.total = cartItem.sell_price * newQuantity;
    cartItem.quantity = newQuantity;
  },
  deleteProducts(state, { id }) {
    var index = state.items.indexOf(id);
    state.items.splice(index, 1);
  },
  loadProducts: (state, data) => {
    state.products = data;
  },
  loadInvoice: (state, data) => {
    state.invoice = data;
  },
  loadOrderPrint: (state, data) => {
    state.order = data;
  },
  loadPaymentMethods: (state, data) => {
    state.paymentMethods = data;
  },
  loadPaymentTypes: (state, data) => {
    state.paymenttypes = data;
  },
  loadEmployees: (state, data) => {
    state.employees = data;
  },
  loadClients: (state, data) => {
    state.clients = data;
  },
  loadInvoiceConfig: (state, invoice) => {
    state.invoiceConfig = invoice;
  },
  loadDelivery: (state, delivery) => {
    state.delivery = delivery;
  },
  updateSelectedUser: (state, id) => {
    const sUser = state.contactList.filter(user => user.id == id);
    state.selectedUser = sUser[0];
    // console.log(state.selectedUser);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
