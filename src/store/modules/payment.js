import { Axios } from "@/data/config";

const state = {
  payment: []
};

const getters = {
  Payment: state => state.payment
};

const actions = {
  async getPayments({ commit }, data) {
    const payments = await Axios.get(
      "api/accountreceivablepayments/" + data.receivable_payment_id
    );

    await commit("setPayment", payments.data);
  },

  async lastPayment() {
    const lastPay = await Axios.get("/api/lastpayment");

    return lastPay.data;
  }
};

const mutations = {
  setPayment(state, data) {
    console.log(data);
    state.payment = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
