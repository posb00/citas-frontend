import { Axios } from "@/data/config";

const state = {
  sellReportData: []
};

const getters = {
  sellReportData: state => state.sellReportData
};

const actions = {
  async sellReport({ commit }, data) {
    const sells = await Axios.post("api/sellreport", { data: data });
    return sells;
  }
};

const mutations = {
  setSellReport(state, data) {
    state.sellReportData = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
